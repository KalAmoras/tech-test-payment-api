using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using tech_test_payment_api.Context;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VendaController : ControllerBase
    {
        private readonly VendaContext _context;

        public VendaController(VendaContext context)
        {
            _context = context;
        }

        /*/// <summary>
        /// Lista as vendas.
        /// </summary>
        /// <parameter>Sem parâmetros</parameter>
        /// <returns>Os itens da lista de vendas</returns>
        /// <response code="200">Retorna as vendas cadastrados</response>
        // GET: api/Venda
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Venda>>> GetTabelaVenda()
        {
            return await _context.TabelaVenda.ToListAsync();
        }*/



        /// <summary>
        /// Busca venda por ID.
        /// </summary>
        /// <parameter>Insira o número de ID</parameter>
        /// <returns>Retorna item buscado por ID</returns>
        /// <response code="200">Retorna o item cadastrado</response>
        // GET: api/Venda/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Venda>> GetVenda(int id)
        {
            var venda = await _context.TabelaVenda.FindAsync(id);

            if (venda == null)
            {
                return NotFound();
            }

            return venda;
        }               
        
        

                        
        /// <summary>
        /// Altera o status da venda por ID.
        /// </summary>
        ///<remarks>
        /// Dificuldade para criar condicional que funcione com In-Memory
        ///</remarks>
        /// <parameter>Insira o número de ID</parameter>
        /// <returns>Retorna item buscado por ID</returns>
        /// <response code="200">Retorna o item cadastrado</response>
        [HttpPut("{id}/statusVenda")]
        public async Task<IActionResult> PutVenda(int id, Venda venda)
        {
            if (id != venda.Id)
            {
                return BadRequest();
            }
            
            //Mantém as linhas com os valores diferentes do Status imutáveis
            _context.TabelaVenda.Attach(venda);
            _context.Attach(venda).Property(x => x.Status).IsModified = true;
        
            //Modifica toda entidade
            //_context.Entry(venda).State = EntityState.Modified;
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!VendaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }



        /// <summary>
        /// Cadastra uma venda.
        /// </summary>
        /// <parameter>Insira o número de ID</parameter>
        /// <returns>Retorna item buscado por ID</returns>
        /// <response code="200">Retorna o item cadastrado</response>
        // POST: api/Venda
        [HttpPost]
        public async Task<ActionResult<Venda>> PostVenda(Venda venda)
        {
            _context.TabelaVenda.Add(venda);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetVenda", new { id = venda.Id }, venda);
        }


        /*/// <summary>
        /// Deleta venda por ID.
        /// </summary>
        /// <parameter>Insira o número de ID</parameter>
        // DELETE: api/Venda/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteVenda(int id)
        {
            var venda = await _context.TabelaVenda.FindAsync(id);
            if (venda == null)
            {
                return NotFound();
            }

            _context.TabelaVenda.Remove(venda);
            await _context.SaveChangesAsync();

            return NoContent();
        }*/

        private bool VendaExists(int id)
        {
            return _context.TabelaVenda.Any(e => e.Id == id);
        }
    }
}
