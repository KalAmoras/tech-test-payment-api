using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace tech_test_payment_api.Models
{
    //Uma venda contém informação sobre o vendedor que a efetivou, data, identificador do pedido e os itens que foram vendido

    public class Venda 
    {
        [Key]
        public int Id { get; set; }
        public string Produto { get; set; }
        public DateTime Data { get; set; }
        public EnumStatusVenda Status { get; set; }
        public string CpfVendedor { get; set; }
        public string NomeVendedor { get; set; }
        public string Telefone { get; set; }
        public string Email { get; set; }   
        
            
    }

       
}